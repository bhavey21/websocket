import express from 'express';
import { Server } from 'socket.io';
const app = express();




const server = app.listen(3001,()=>{
    console.log('Server is running');
})

const io = new Server(server, { cors: { origin: '*' } });

let connectedCount = 0;
io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
        console.log('disconnected')
        socket.disconnect()
    });
  socket.on('subscribe', async ()=>{
    connectedCount++;
      setTimeout(()=>{
        socket.emit('response',{
            type: 'Subscribe',
            status: "Subscribed",
            updatedAt: new Date()
            })
      },4000)
    });

    socket.on('unsubscribe', async ()=>{
        if(connectedCount>0){
          connectedCount--;
          setTimeout(()=>{
            socket.emit('response',{
                type: 'Unscubscribe,',
                status: "Unsubscribed",
                updatedAt: new Date()
                })
          },8000)
        }
        });

        socket.on('countsubscribers', async ()=>{
          socket.emit('response',{
                  type: 'CountSubscribers',
                  count: connectedCount,
                  updatedAt: new Date()
                  })
            })

            socket.onAny((event,args)=>{
              if(event!='subscribe' && event!='unsubscribe' && event!='countsubscribers'){
                socket.emit('response',{
                  type: "Error",
                  error: "Requested method not implemented",
                  updatedAt: new Date()
                  })
              }
            })
  });