import io from 'socket.io-client';
import './App.css';
import {useEffect, useState} from 'react'
console.log(process.env.REACT_APP_SERVER_URL)
const socket = io.connect(process.env.REACT_APP_SERVER_URL);
function App() {
 const [response,setRespose] = useState(null)
  useEffect(()=>{
    const subscribeResponse = () => {
      if(socket){
        socket.on('response',(data)=>{
          setRespose(JSON.stringify(data))
        })
      }
    }
    subscribeResponse();
  },[socket])

  const emitSubscribe = ()=>{

    socket.emit('subscribe')
  }

  const emitUnsubscribe = ()=>{
    socket.emit('unsubscribe')
  }

  const emitCountSubscriber = ()=>{
    socket.emit('countsubscribers')
  }

  const emitAnyOther = ()=>{
    socket.emit('emitAnyOther')
  }

  return (
    <div className='App'>
      <br/>
      <button onClick={emitSubscribe}>Subscribe</button> &nbsp;
      <button onClick={emitUnsubscribe}>UnSubscribe</button> &nbsp;
      <button onClick={emitCountSubscriber}>Count Subscriber</button> &nbsp;
      <button onClick={emitAnyOther}>Any other</button>
      <br/><br/>
      {response?response:''}
    </div>
  );
}

export default App;
